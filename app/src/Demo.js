import React from 'react';
import './Default.css'
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Progress from './Progress';
 
function Demo () {
    return (
        <Container fluid className='pt-2 text-left'>
            <Row>
                <Col>
                    <h5>Select a Doctor</h5>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className='Default-box'></div>
                </Col>
            </Row>
            <Row className='pt-2'>
                <Col>
                    <h3><strong>DR. WHO</strong></h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fermentum imperdiet varius scelerisque sed risus urna bibendum diam nunc.
                    </p>
                </Col>
            </Row>
            <Row>
                <Col className="text-center">
                    <Button variant="outline-light">That's Me!</Button>
                </Col>
            </Row>
            <Row className='pt-5'>
                <Col>
                    
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className='Default-box Default-box-outline'>
                        <div className='Default-box Default-box-grey'>Default-box-grey</div>
                        Default-box-outline
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className='Default-circle'>Default-circle</div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className='Default-box pt-2'>
                        <Progress value={.85} />
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default Demo;