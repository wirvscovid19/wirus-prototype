import React from 'react';
import { geolocated } from "react-geolocated";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import './Default.css';
import './Training.css';
import Progress from './Progress';
import FloorImg from './Floor.png';
import DRTUBE from './DRTUBE.gif';
import EnergyIcon from './EnergyIcon.png';

class Training extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lastLon: 0,
            lastLat: 0,
            startLon: 0,
            startLat: 0,
            distanceToLast: 0,
            distanceToStart: 0,
            points: 0,
            timeSinceStart: '',
            intervalID: null,
            // dates: [
            //   {
            //     Date: new Date(),
            //     Energy: 123,
            //   },
            //   {
            //     Date: new Date(Date.now() - 1 * 86400000),
            //     Energy: 453,
            //   },
            //   {
            //     Date: new Date(Date.now() - 2 * 86400000),
            //     Energy: 300,
            //   },
            // ],
        };
    }

    componentDidMount() {
      const intervalID = setInterval(this.pointLoop.bind(this), 5000);
      this.setState({
        intervalID:intervalID}
      )
    }

    componentWillUnmount() {
      clearInterval(this.state.intervalID);
    }

    calculateDistance(lon1, lat1, lon2, lat2) { // based heavily on https://stackoverflow.com/a/27943
        const R = 6371; // Radius of the earth in km
        const deg2rad = (Math.PI/180);
        const dLatDiv2 = deg2rad*(lat2-lat1) / 2;  // deg2rad below
        const dLonDiv2 = deg2rad*(lon2-lon1) / 2;
        var a = 
            Math.sin(dLatDiv2) * Math.sin(dLatDiv2) +
            Math.cos(deg2rad * lat1) * Math.cos(deg2rad * lat2) * 
            Math.sin(dLonDiv2) * Math.sin(dLonDiv2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c * 1000; // Distance in m
    }

    pointLoop() {
      if (this.props && this.props.coords) {
        const distanceToStart = this.calculateDistance(this.props.coords.longitude, this.props.coords.latitude, this.props.startCoords.longitude, this.props.startCoords.latitude);
        const points = this.state.points + Math.max(this.calculatePoints(distanceToStart) * 83,25);
        const timeSinceStart = new Date(Date.now() - this.props.startTime).toISOString().substr(11, 8);
        const difference = Math.floor(points) - Math.floor(this.state.points);
        this.spawnEnergy(difference, ()=>{
          this.setState({
            points: points,
            distanceToStart: distanceToStart,
            timeSinceStart: timeSinceStart,
          });
        });

      }
    }

    spawnEnergy(amount, callback) {
      const energyElem = document.getElementById("EnergyElement");
      const energyElemText = document.getElementById("EnergyElementText");
      const startP = this.getScreenCordinates(document.getElementById("EnergyStartMarker"));
      const endP = this.getScreenCordinates(document.getElementById("EnergyStopMarker"));
      energyElem.classList.add('no-transitions');
      energyElem.style.left = startP.x + 'px';
      energyElem.style.top = startP.y + 'px';
      energyElem.style.opacity = 1;
      energyElemText.textContent = (amount >= 0 ? '+' : '-') + '' + amount;
      setTimeout(() => {
        energyElem.classList.remove('no-transitions');
        energyElem.style.left = endP.x + 'px';
        energyElem.style.top = endP.y + 'px';
        energyElem.style.opacity = 0;
      }, 250);
      setTimeout(callback, 2000);
    }

    getScreenCordinates(obj) {
        var p = {};
        p.x = obj.offsetLeft;
        p.y = obj.offsetTop;
        while (obj.offsetParent) {
            p.x = p.x + obj.offsetParent.offsetLeft;
            p.y = p.y + obj.offsetParent.offsetTop;
            if (obj === document.getElementsByTagName("body")[0]) {
                break;
            }
            else {
                obj = obj.offsetParent;
            }
        }
        return p;
    }

    calculatePoints(distance) {
      const maxdistance = 10;
      const mindistance = 0;
      distance = Math.max(distance, mindistance);
      return Math.max(0,(maxdistance - distance) / (maxdistance - mindistance));
    }

    componentDidUpdate(prevProps) {
        if (prevProps 
            && prevProps.coords
            && (prevProps.coords.longitude !== this.state.lastLon || prevProps.coords.latitude !== this.state.lastLat)
        ){
          const distanceToLast = this.calculateDistance(this.props.coords.longitude, this.props.coords.latitude, this.state.lastLon, this.state.lastLat);

          this.setState(
            {
                lastLon: prevProps.coords.longitude,
                lastLat: prevProps.coords.latitude,
                distanceToLast: distanceToLast,
            }
          )
        }
    }

    render() {

        if (!this.props.isGeolocationAvailable){
            return(
              <Container fluid className='pt-2 text-left'>
                <Row>
                    <Col>
                      <Alert variant="warning">
                        <Alert.Heading>Hey, nice to see you</Alert.Heading>
                        <p>
                          Your browser does not support Geolocation.
                        </p>
                        <hr />
                        <p className="mb-0">
                          Please enable your Geolocation. (Ortungsdienste)
                        </p>
                      </Alert>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                      <h5>Don't care, take me directly to the battle:</h5>
                    </Col>
                </Row>
                <Row className='pt-3'>
                  <Col className="text-center">
                      <Button variant="outline-light" onClick={()=>this.props.onBattle(this.state.points)}>Battle</Button>
                  </Col>
              </Row>
              </Container>
            );
        } else if(!this.props.isGeolocationEnabled){
            return(
              <Container fluid className='pt-2 text-left'>
                <Row>
                    <Col>
                      <Alert variant="warning">
                        <Alert.Heading>Hey, nice to see you</Alert.Heading>
                        <p>
                          Geolocation is not enabled
                        </p>
                        <hr />
                        <p className="mb-0">
                          Please enable your Geolocation. (Ortungsdienste)
                        </p>
                      </Alert>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                      <h5>Don't care, take me directly to the battle:</h5>
                    </Col>
                </Row>
                <Row className='pt-3'>
                  <Col className="text-center">
                      <Button variant="outline-light" onClick={()=>this.props.onBattle(this.state.points)}>Battle</Button>
                  </Col>
              </Row>
              </Container>
            );
        }else if (this.props.coords){
            // const Energies = this.state.dates.map(x => x.Energy);
            // const maxEnergy = Math.max(Energies);
            // const minEnergy = Math.min(Energies);
            // const listItems = this.state.dates.map((x) => <li><DateAndEnergy Date={x.Date} Energy={x.Energy} maxEnergy={maxEnergy} minEnergy={minEnergy} /></li>)
            const progress = this.state.points / this.props.energyGoal;
            if(progress >= 1.0){
              setTimeout(()=>{
                  this.props.onBattle(this.state.points)
                }
              ,2000)}
            const distanceFactor = (1 - this.calculatePoints(this.state.distanceToStart));
            return(
              <div>
                <div id='EnergyElement'><span id="EnergyElementText">0</span><img src={EnergyIcon} alt='energyIcon'/></div>
                <Container fluid className='pt-2 text-left Default-foreground'>
                  <Row className='pt-4'>
                      <Col className='pr-0'>
                        <div className='Default-box Training-box-top no-right-radius pb-0 pt-4 ml-5'>
                          <span className='pr-3 Training-label'>Rank</span>
                          <h3 className='Default-red-text d-inline text-capitalize'><strong>{this.props.rank}</strong></h3>
                          <div id='EnergyStopMarker'></div>
                          <Progress className='mt-2' value={progress} />
                          {/* <div className='Default-circle Default-circle-border' id='WeaponIcon'></div> */}
                        </div>
                      </Col>
                  </Row>
                </Container>
                <Container fluid>
                  <Row>
                    <Col className='p-0'>
                      <div id="DistanceOMeter">
                        <div id="DistancePlayer" style={{bottom: ( -distanceFactor * 200) + "px", left: "calc(50% + "+ distanceFactor * 120 + "px)"}}>
                          <img src={DRTUBE} alt="DR.TUBE"/>
                          <div id='EnergyStartMarker'></div>
                        </div>
                        <img id="Floor" src={FloorImg} alt="Floor"/>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </div>
            );
        }else{
            return(
              <Alert variant="light">
                Getting location data ...
            </Alert>
            )
        }
    }
}

export default geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    // watchPosition:true,
    userDecisionTimeout: 5000,
})(Training);