import React from 'react';
import './index.css';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Floor from './Floor';
import Progress from './Progress';
import DRTUBE from './DRTUBE.gif';
import VIRUS from './virus.gif';
import VIRUSATTACK from './virusAttack2.gif';
import RIP from './RIP.png';
import ATTACK from './attack.png';
import MYATTACK from './myAttack.gif';
import ATTACKCOOLDOWN from './attackCooldown.png';


function Virus(props){
    const scalingFactor = 1.0;//props.cases.germany/25000.0;
    const size = scalingFactor*170.0;
    const styles = 
        {
            box:{
                width: size+"px",
                position:"absolute",
                left:props.left?props.left:"55%",
                top:props.top?props.top:"-70px",
                opacity:props.dead?1:props.hit?0:0.8
            },
            virus:{
                width: size+"px",
            }

    }
    return(
        <div style={styles.box}>
            <div hidden={props.dead} >{props.cases.germany===1?"?":props.cases.germany} infected <br/>people in Germany.</div>
            <div>{props.children}</div>
            <img style={styles.virus} src={props.dead?RIP:props.attack?VIRUSATTACK:VIRUS} alt="virus"/>
        </div>
    )
}

class Myself extends React.Component
{
    render(){
        const points=this.props.points?this.props.points:10;
        const scalingFactor = points/10.0;
        const width = scalingFactor*130.0*(this.props.attack?1.0:0.7942);
        //const height = width*1.5;
        const styles = 
        {
            box:{
            width: width+"px",
            position:"absolute",
            left:"calc("+(this.props.left?this.props.left:50)+"% - "+width/2+"px)",
            bottom:this.props.attack?"-10px":"-20px",
            opacity:this.props.hit?0:1
            },
            myself:{
                width: width+"%",
            }
        
        }

        return(
            <div style={styles.box}>
                <img style={styles.myself} src={this.props.dead?RIP:this.props.attack?MYATTACK:DRTUBE} alt="DR.TUBE"/>
            </div>
        )

    }
    
}



class CombatView extends React.Component {
    constructor(props){
        super(props)
        this.state={
            cases:{
                germany:1,
            },
            myself:{
                points: 10,
                maxPoints: 100,
                lifes: 100,
                maxLifes: 100,
                hit: false,
                attack:false,
                dead:false
            },
            virus:{
                points: 3,
                maxPoints: 100,
                lifes: 100,
                maxLifes: 100,
                attackSpeed: 6000,
                hit: false,
                attack:false,
                dead:false
            },
            weapon:{
                cooldown:false
            }
        }
    }
    virusAttackLoop(){
        const myself =  Object.assign({}, this.state.myself);
        const virus =  Object.assign({}, this.state.virus);
        if (virus.lifes > 0){
            //myself.hit=true;
            virus.attack=true;
        }
        this.setState({
            myself:myself,
            virus:virus
        })
        setTimeout(()=>{
            const myself =  Object.assign({}, this.state.myself);
            const virus =  Object.assign({}, this.state.virus);
            if (virus.lifes > 0){
                myself.lifes =  Math.max(myself.lifes-virus.points,0)
            }
            virus.attack=false;
            myself.hit=false;
            this.setState({
                myself:myself,
                virus:virus
            })
            if (myself.lifes > 0){
                setTimeout(()=>this.virusAttackLoop(), Math.ceil(Math.random()*virus.attackSpeed));
            }else{
                    const myself =  Object.assign({}, this.state.myself);
                    myself.lifes=0;
                    myself.dead=true;
                    this.setState({
                            myself:myself
                    })
                    return;
            }
            ;
        }, 2400)
        
    }
    myAttack(){
        const weapon =  Object.assign({}, this.state.weapon);
        if(!weapon.cooldown){
            const myself =  Object.assign({}, this.state.myself);
            const virus =  Object.assign({}, this.state.virus);
            if (myself.lifes > 0){
              //  virus.hit=true;
                myself.attack=true;
                weapon.cooldown=true;
            }
            this.setState({
                myself:myself,
                virus:virus,
                weapon:weapon
            })
            setTimeout(()=>{
                const weapon =  Object.assign({}, this.state.weapon);
                weapon.cooldown=false;
                const virus =  Object.assign({}, this.state.virus);
                const myself =  Object.assign({}, this.state.myself);
                if (myself.lifes > 0){
                    virus.lifes =  Math.max(virus.lifes-myself.points,0)
                }
                myself.attack=false;
                virus.hit=false;
                this.setState({
                    virus:virus,
                    myself:myself,
                    weapon:weapon
                })
                if (virus.lifes <= 0){
                    const virus =  Object.assign({}, this.state.virus);
                    virus.lifes=0;
                    virus.dead=true;
                    this.setState({
                            virus:virus
                    })
                    setTimeout(()=>this.props.afterBattle(),5000);
                    return;
                }
            }, 2400)


        }
    }
    componentDidMount(){
        axios.get('https://coronavirus-19-api.herokuapp.com/countries/germany')
        .then(
            response => {
                if(response.data.active
                    && !isNaN(response.data.active)){
                        this.setState(
                            {
                                cases:{
                                    germany:response.data.active
                                }
                            }
                        )
                    }

            })
        /* Under construction
        axios.get('https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/ef4b445a53c1406892257fe63129a8ea_0/geoservice?geometry=-22.854%2C46.270%2C39.109%2C55.886')
        .then(
                response => {
                    try {      
                    } catch (error) {
                        console.log(response.dat)
                        const state = response.data.features[10].attributes["LAN_ew_GEN"]   
                    }
                })*/
        setTimeout(()=>this.virusAttackLoop(), Math.ceil(Math.random()*this.state.virus.attackSpeed))
    }
    render() {
        const floorRadius=300;
        const styles={
            battle:{
                position:"fixed",
                bottom:"200px",
                width:"100%",
                maxWidth:"100%"
            },
            floor:{
                left:"calc(50% - "+floorRadius+"px)"
            },
            box:{
                position:"absolute",
                color:"#fc4e4b",
                right:0
            },
            statusbar:{
                position:"fixed",
                bottom:"50px"
            },
            weapon:{
                heated:0,
            }
        }
        return(
        <div>
            <Container style={styles.battle} className='pt-2 text-left'>
                <Row>
                    <Col>
                        <Floor style={styles.floor} diameter={floorRadius*2}>
                            <Virus cases={this.state.cases} hit={this.state.virus.hit}  attack={this.state.virus.attack} dead={this.state.virus.dead}>
                                <Progress color="#3D1556" className='mt-2' desc="Health" value={this.state.virus.lifes / 100} />
                            </Virus>
                            <Myself points={this.state.myself.points} hit={this.state.myself.hit}  attack={this.state.myself.attack} dead={this.state.myself.dead}/>
                        </Floor>
                    </Col>
                </Row>
            </Container>
            <Container style={styles.statusbar} fluid className='pt-2 text-left'>
                <Row className='pt-4'>
                    <Col className='pr-0'>
                      <div className='Default-box Training-box-top no-right-radius pb-0 pt-4 ml-5'>
                        <span className='pr-3 Training-label'>Rank</span>
                        <h3 className='Default-red-text d-inline text-capitalize'><strong>Newbie I</strong></h3>
                        <Progress className='mt-2' desc="Health" value={this.state.myself.lifes / 100} />
                        <img className='Default-circle' id='WeaponIcon' src={this.state.weapon.cooldown?ATTACKCOOLDOWN:ATTACK} alt='attack' onClick={()=>this.myAttack()}/>
                      </div>
                    </Col>
                </Row>
              </Container>
        </div>
        )
    }
}

export default CombatView;
