import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import CombatView from './CombatView'
import Training from './Training';
import CharacterSelection from './CharacterSelection';
import { geolocated } from "react-geolocated";
//import Button from 'react-bootstrap/Button';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: '',
      startCoords: null,
      startTime: null,
    };
  }

  setPageToTraining() {
    let coords = null;
    if (this.props.isGeolocationAvailable &&
        this.props.isGeolocationEnabled &&
        this.props.coords) {
      coords = this.props.coords;
    }
    this.setState({
      currentPage: 'Training',
      startCoords: coords,
      startTime: Date.now(),
    });
  }
  
  setPageToCombatView(points) {
    console.log(points)
    this.setState({
      currentPage: 'CombatView'
    });
  }

  render() {
    let page = null;
    switch (this.state.currentPage) {
      case 'Training':
        page = <Training onBattle={(points)=>{this.setPageToCombatView(points)}} startCoords={this.state.startCoords} startTime={this.state.startTime} energyGoal={400} rank='Newbie'/>
        break;
      case 'CombatView':
        page = <CombatView afterBattle={()=>this.setPageToTraining()}/>
        break;
      default:
        page = <CharacterSelection onSelected={()=>this.setPageToTraining()}/>
        break;
    }
    return (
      <div className="App">
        <header className="App-header">
        </header>
        {page}
      </div>
    );
  }
}

export default geolocated({
  positionOptions: {
      enableHighAccuracy: true,
  },
  userDecisionTimeout: 5000,
})(App);
