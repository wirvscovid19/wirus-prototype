import React from 'react';
import './index.css';

function Floor(props){
    function Circle(props){
        const style = {
                backgroundColor: "white",
                opacity: 0.2,
                borderRadius: "50%",
                position:"absolute",
                width:props.diameter,
                paddingTop:props.diameter,
                top:props.top?props.top:0,
                left:props.left?props.left:0
            }
        return(
            <div style={style}></div>
        )
    }
    function RotationBox(props){
        const style = {
            transform: "rotateX("+(props.angle?props.angle+"deg":"0deg")+")",
            position:"relative",
            width:props.diameter+"px",
            height:props.diameter+"px",
            top:(sky-props.diameter/2)+"px"
        }
        return(
            <div style={style}>
                {props.children}
            </div>
        )
    }
    function BoundingBox(props){
        let style = {
            position:"relative",
            width:props.diameter+"px",
            height:props.sky+"px",
        }
        if (props.style){
            style = Object.assign({}, style, props.style);
        }
        return(
            <div style={style} className={props.className} id={props.id}>
                {props.children}
            </div>
        )
    }

    const angle = props.angle?props.angle:70
    const diameter = props.diameter?props.diameter:500
    const sky= props.sky?props.sky:props.diameter/2



    return(
        <BoundingBox id={props.id} className={props.className} style={props.style} diameter={diameter} sky={sky}>
            <RotationBox diameter={diameter} angle={angle} sky={sky}>
                <Circle diameter="100%" top="0" left="0"></Circle>
                <Circle diameter="80%" top="10%" left="10%"></Circle>
                <Circle diameter="60%" top="20%" left="20%"></Circle>
            </RotationBox>
            {props.children}
        </BoundingBox>
    )
    
}

export default Floor;