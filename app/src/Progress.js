import React from 'react';
import './Progress.css'
 
function Progress (props) {
    const percent = Math.max(0, Math.min(100, props.value * 100));
    const styles={
      bar:{
        color: props.color?props.color:"#fc4e4b",
      },
      indicator:{
        width: (percent) + "%",
        backgroundColor: props.color?props.color:"#fc4e4b",
      }
    }
    return (
        <div style={styles.bar} className={'Progress-bar' + (props.className ? ' ' + props.className : '')}>
          <div style={styles.indicator} className='Progress-indicator'></div>
          {Math.floor(percent)}% {props.desc?props.desc:"Complete"}
        </div>
    );
}

export default Progress;