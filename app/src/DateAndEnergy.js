import React from 'react';
import './Default.css';
import './DateAndEnergy.css';
 
function DateAndEnergy (props) {
    // props = {
    //   Energy: 453,
    //   Date: new Date(),
    //   maxEnergy: 513,
    //   minEnergy: 123,
    // }
    const maxTop = 0; // %
    const minTop = 75; // %
    const dayNameShort = props.Date.toString().split('')[0];
    const alpha = Math.min(1, Math.max(0, (props.Energy - props.minEnergy) / (props.maxEnergy - props.minEnergy)));
    const top = alpha * maxTop + (1 - alpha) * minTop;
    return (
        <div className={'DateAndEnergy-container Default-box' + (props.className ? ' ' + props.className : '')} style={{top: top}}>
          <div className='Default-box Default-box-grey'>{props.Energy >= 0 ? '+' : '-'}{props.Energy} Energy</div>
          <h2 className={props.toDateString() == new Date().toDateString() ? 'Default-black-text' : 'Default-red-text' }>{props.Date.getDate()}</h2>
          {dayNameShort}
        </div>
    );
}

export default DateAndEnergy;