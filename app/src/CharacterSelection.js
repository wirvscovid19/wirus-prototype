import React from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Carousel from 'react-bootstrap/Carousel';
import './Default.css';
import './CharacterSelection.css';
import DRTUBE from './DRTUBE.gif';
import DRSHO from './DRSHO.png';

function ControlledCarousel(props) {
  const index = props.index;

  const handleSelect = (selectedIndex, e) => {
    props.onChange(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect} interval={null}>
      <Carousel.Item>
          <div className='CharacterSelection-container'>
            <div className='Default-box'>
            </div>
            <img
              src={DRTUBE}
              alt="Dr. Tube"
            />
          </div>
        <Carousel.Caption>
            <h3 className='pt-3'><strong>DR. TUBE</strong></h3>
            <p>
              Dr. Tube is always prepared. Her suit doesn't show that she is actually pretty hot. But don't let this fool you into thinking she isn't a great scientist!
            </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
          <div className='CharacterSelection-container'>
            <div className='Default-box'>
            </div>
            <img
              src={DRSHO}
              alt="Dr. Sho"
            />
          </div>
        <Carousel.Caption>
            <h3 className='pt-3'><strong>DR. SHO</strong></h3>
            <h5>(COMING SOON)</h5>
            <p>
              Dr. Sho knows his way around viruses in Afrika. He has spent most of his carreer fighting viruses like Ebola in the third world.
            </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

class CharacterSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    }
  }

  handleIndexChange(index) {
    this.setState({index: index});
  }

  render () {
    return (
      <Container className='pt-2 text-left'>
      <Row>
          <Col>
              <h5>Select your Hero:</h5>
          </Col>
      </Row>
      <Row>
          <Col>
              <ControlledCarousel index={this.state.index} onChange={this.handleIndexChange.bind(this)}/>
          </Col>
      </Row>
      <Row className='pt-3'>
          <Col className="text-center">
              <Button variant="outline-light" onClick={()=>this.props.onSelected()}>That's Me!</Button>
          </Col>
      </Row>
    </Container>
    );
  };
}

export default CharacterSelection;