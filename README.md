# README #

## Stay'N'Fight ##

### What is this repository for? ###

see also [devpost](https://devpost.com/software/id-1938-informationsflusses-vom-test-zum-testergebnis). *#WirVsVirusHack*


#### Inspiration

Die Anzahl der SARS-CoV-2-infektionen und Toten steigt. Die hohe Ansteckungsgefahr ist bekannt.

Doch die oft milden Verläufe verleiten viele Menschen dazu, nicht viel auf die Ermahnungen zum Social disctancing zu geben.

Die Greifbarkeit dieses sehr kleinen aber gefährlichen Gegners scheint ein großes Problem zu sein. Daher dachten wir uns, wir machen ihn sichtbar und und zum angreifbaren Gegner. Den man nur mit sozial kompatiblem Verhalten aka zu Hause bleiben besiegen kann.

#### Was macht das Spiel?

Mit unserem Spiel wird man zum Seuchenspezialist der gegen die infektiösesten und gefährlichsten Viren kämpft! Doch man kann nicht einfach so starten. So ein Virus ist stark und wird mit steigenden Infektionszahlen stärker!

Du brauchst Energie, um gegen den Virus anzukommen. Die bekommst du nur, wenn du zu Hause bleibst und wortwörtlich deine Akkus auflädst.

Zusätzlich bekommst du Energie, indem du Challenges absolvierst, die dir und auch anderen ein gutes Gefühl geben, wie deine Omi anzurufen oder dich mit deinen Freunden auf ein digitales Kaffeedate zu treffen. Deine Freunde können dir auch challenges stellen und sie entscheiden dann, ob du die Energie bekommst.

Deine Energie ist auch deine Währung, die dir dein Equipment ermöglicht. Je mehr Energie, desto besser!

Irgendwann musst du dich den Viren stellen! Um gegen den Endgegener SARS-Cov-2 gewappnet zu sein, kämpfst du gegen neun schwächere Viren und steigst in der Profession vom Newbie zum Virus Evangelist auf.

Je mehr Leute sich an die Vorgaben halten, desto besser wird die Chance den Endgegner – das Coronavirus SARS-COV-2 – schlagen zu können! Wenn du es besiegst wirst du zum SARS-COV-2-Slayer!

Also *Stay'N'Fight!*

**Erklärung zu den Links**

- https://staynfight.lukius.de/: Hier ist der aktuelle ausführbare Stand des Spiels abgelegt. Die Kernfunktionen, wie Energieerhalt im Zuge des Zu Hause Bleibens und der Combatview mit animierten Charakteren für eine Spieler vs Virus Combination kann ausprobiert werden. Bitte daran denken die Standortdienste zu aktivieren, sonst funktioniert es nicht.
- https://bitbucket.org/wirvscovid19/wirus-prototype/src/master/: Hier ist der Sourcecode abgelegt.

- https://www.figma.com/proto/wugLFpNasgAPNAOrF9Brvw/Wireframe?node-id=96%3A2093&scaling=scale-down: Hier ist ein Klick-Prototyp, um das Konzept ausprobieren zu können.

**Built With:**

- bitbucket
- css
- docker
- figma
- html
- javascript
- love
- node.js
- npm
- react
- Try it out


### Helpful Data Sources for Corona ###

##### Datasources:

- Landkreise: https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0/data
- Bundeländer: https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/ef4b445a53c1406892257fe63129a8ea_0?geometry=-22.854%2C46.270%2C39.109%2C55.886
- Welt:https://github.com/javieraviles/covidAPI/blob/master/README.md


https://services9.arcgis.com/N9p5hsImWXAccRNI/


##### Dashboards: 
- Landkreis:https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4/page/page_1/
- Bundesland: https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4
- Welt:https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6
