FROM node:12-alpine

RUN mkdir -p /app
WORKDIR /app

COPY ./app/ /app
RUN npm install
RUN npm run build

EXPOSE 3000

CMD [ "node_modules/serve/bin/serve.js", "-s", "build" ]
